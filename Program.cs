﻿using System;
using System.Linq;
using System.Threading;
using Zoology.Animals;

namespace Zoology
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("\nHello ZooWorld!\nWe've got a great zoo but no animals. Let's add some.");

            while(ProcessTurn());

            Console.WriteLine("\nGame over.");
        }

        static bool ProcessTurn()
        {
            Zoo Zoo = new Zoo();

            Console.WriteLine($"\nWe can add lions and tigers and bears and elephants. What would you like?");
            Animal choice = FindAnimalByStringInput(Console.ReadLine());

            if(choice == null)
            {
                Console.WriteLine("That's not a valid animal...");
            }
            else
            {
                if(Zoo.AddAnimal(choice))
                {
                    Console.WriteLine("The animal has been added to the zoo.");
                }
            }

            return CheckIfZooIsFine(Zoo);
        }

        static Animal FindAnimalByStringInput(string animal)
        {
            switch(animal.ToLower())
            {
                case "elephant": return new Elephant();

                case "tiger": return new Tiger();

                default: return null;
            }
        }

        static bool CheckIfZooIsFine(Zoo Zoo)
        {
            if(Zoo.Animals.Any(a => a.Dangerous))
            {
                Console.WriteLine("\n\nUhm...");
                Thread.Sleep(1400);

                Console.WriteLine("\n.. It seems you've added some animals that are really bad news.");
                Thread.Sleep(2500);

                Console.WriteLine("\nYeah. Everyone is dead and the zoo is burning. Nice job.");
                Thread.Sleep(1000);

                return false;
            }
            
            return true;
        }
    }
}
