namespace Zoology.Animals
{
    abstract class Animal 
    {
        public readonly bool Dangerous = false;
        public string Name { get; set; }
        public int Height { get; set; }
        public int Weight { get; set; }

        public Animal() {}

        public Animal(bool dangerous) => Dangerous = dangerous;

        public Animal(int height, int weight) 
        {
            Height = height;
            Weight = weight;
        }

        public Animal(string name, int height, int weight) : this(height, weight)
        {
            Name = name;
        }
    }
}