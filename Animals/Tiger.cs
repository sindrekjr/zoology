using System;

namespace Zoology.Animals
{
    class Tiger : Animal, IMove
    {
        

        public Tiger() : base(new Random().Next(0, 10) > 2) {}

        public void Move(int distance)
        {
            // The tiger is too stealthy for us to realise when it
            // moves, therefore there is no output
        }

        
    }
}