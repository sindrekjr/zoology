using System;

namespace Zoology.Animals
{
    class Elephant : Animal, IMove 
    {
        
        public Elephant() : this(50, 1000) {}
        public Elephant(int height, int weight) : this("Elephant Elephant", height, weight) {}
        public Elephant(string name, int height, int weight) : base(name, height, weight) {}


        public void Move(int distance)
        {
            Console.WriteLine($"{Name} moved {distance}. Meters? Feet? Who knows.");
        }

        public void Stomp()
        {
            Console.WriteLine($"{Name} stomps! :O :O");
        }
    }
}