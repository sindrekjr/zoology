namespace Zoology
{
    interface IMove
    {
        public void Move(int distance);
    }
}