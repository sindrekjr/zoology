using System;
using System.Collections.Generic;
using Zoology.Animals;

namespace Zoology
{
    class Zoo
    {
        public List<Animal> Animals { get; set; }

        public Zoo(List<Animal> animals = null)
        {
            Animals = animals ?? new List<Animal>();
        }

        public bool AddAnimal(Animal animal)
        {
            if(!animal.Dangerous || animal.Dangerous && Warn())
            {
                Animals.Add(animal);
                return true;
            }
            return false;
        }

        private bool Warn()
        {
            Console.Write($"\n!! If you add a dangerous animal, it might kill everyone. Are you sure? [y/n]");
            return Console.ReadLine().ToLower().Contains('y');
        }
    }
}
